# kubernetes-aws-eks



## Getting started

Create an Amazon EKS cluster and configure with node groups


### Create a new namespace:

`kubectl create ns php`


### Create a service account:

`kubectl appy -f php/dev-sa.yaml`

### Create deployment:

`kubectl appy -f php/php-deployment.yaml`


### Create load balancer service

`kubectl expose deployment nginx-deployment --namespace=php --type=LoadBalancer --name=nginx-service`

Go to aws loadbalancer section and forward to the dns endpoint.
